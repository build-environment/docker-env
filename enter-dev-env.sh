#!/bin/bash

WORKSPACE_PATH=$(realpath "$(dirname "$(readlink "$0" || realpath "$0")")")

bash -i -e -c "
    if [ ! -d \"$WORKSPACE_PATH/venv\" ] ; then
        virtualenv \"$WORKSPACE_PATH/venv\" ;
    fi ;
    source \"$WORKSPACE_PATH/venv/bin/activate\" ;
    pip install --editable \"$WORKSPACE_PATH\" ;
    bash --norc ;
"
