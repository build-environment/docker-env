# docker-environment

[![license](https://img.shields.io/gitlab/license/build-environment/docker-env)](https://gitlab.com/build-environment/docker-env)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

This python package abstracts the execution of commands inside a docker image.
