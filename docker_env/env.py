# -*- coding: utf-8 -*-

from __future__ import annotations

import logging
import re
import subprocess
from textwrap import indent
from typing import Optional

from . import DockerParam

__all__ = ["DockerEnv"]


class DockerImage:
    __logger = logging.getLogger(__name__)

    def __init__(self, image: str) -> None:
        pattern = re.compile(r"([-\w\.\/]+/)?(?P<image_name>[^:/]+)(:.+)?$")
        groups = pattern.search(image)
        assert groups is not None

        self.__image = image
        self.__name = groups["image_name"]

    def __str__(self) -> str:
        return self.__image

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({str(self)})"

    @property
    def name(self) -> str:
        return self.__name

    def check(self) -> bool:
        cmd = ["docker", "images", "--quiet", self.__image]
        process_result = subprocess.run(
            cmd,
            capture_output=True,
            check=False,
        )
        if process_result.returncode:
            if process_result.stdout:
                self.__logger.error(
                    "stdout:\n%s",
                    indent(process_result.stdout.decode().rstrip(), "> "),
                )
            if process_result.stderr:
                self.__logger.error(
                    "stderr:\n%s",
                    indent(process_result.stderr.decode().rstrip(), "> "),
                )
            # Raise exception
            process_result.check_returncode()

        if process_result.stdout.decode() == "":
            return False

        return True

    def pull(self) -> None:
        cmd = ["docker", "pull", self.__image]
        subprocess.run(
            cmd,
            check=True,
        )

    def run(
        self,
        args: Optional[list[str]] = None,
        cmd: Optional[list[str]] = None,
        capture_output: bool = False,
        check: bool = False,
        timeout: Optional[float] = None,
    ) -> subprocess.CompletedProcess:
        if args is None:
            args = []
        if cmd is None:
            cmd = []

        run_cmd = " ".join(["docker", "run"] + args + [self.__image] + cmd)

        self.__logger.debug(run_cmd)

        process_result = subprocess.run(
            run_cmd,
            capture_output=capture_output,
            shell=True,
            check=False,
            timeout=timeout,
        )

        if process_result.stdout:
            self.__logger.debug(
                "stdout:\n%s",
                indent(process_result.stdout.decode().rstrip(), "> "),
            )
        if process_result.stderr:
            self.__logger.debug(
                "stderr:\n%s",
                indent(process_result.stderr.decode().rstrip(), "> "),
            )

        self.__logger.debug("$ %d", process_result.returncode)

        if check:
            process_result.check_returncode()

        return process_result


class DockerEnv:
    def __init__(
        self,
        image: DockerImage,
        param: Optional[str | list[str] | DockerParam] = None,
    ) -> None:
        self.__image = image
        self.__param = DockerParam(param)

    @property
    def image(self) -> DockerImage:
        return self.__image

    def with_param(self, param: DockerParam) -> DockerEnv:
        return DockerEnv(self.__image, self.__param + param.args)

    def add_param(self, param: DockerParam) -> None:
        self.__param += param

    def __add__(self, param: DockerParam) -> DockerEnv:
        return self.with_param(param)

    def __iadd__(self, param: DockerParam) -> DockerEnv:
        self.add_param(param)
        return self

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({str(self.__image)}[{str(self.__param)}])"

    def run(
        self,
        cmd: Optional[list[str]] = None,
        capture_output: bool = False,
        check: bool = False,
        timeout: Optional[float] = None,
    ) -> subprocess.CompletedProcess:
        return self.image.run(self.__param.args, cmd, capture_output, check, timeout)
