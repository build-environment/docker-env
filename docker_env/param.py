# -*- coding: utf-8 -*-

from __future__ import annotations

import posixpath
import shlex
from collections import abc
from pathlib import Path
from typing import Iterable, Optional

__all__ = ["Param", "EnvVar", "Volume"]


class Param:
    def __init__(self, args: Optional[str | Iterable[str] | Param]) -> None:
        if isinstance(args, str):
            args = [args]
        elif isinstance(args, self.__class__):
            args = args.__args
        elif isinstance(args, abc.Iterable):
            args = list(args)
        elif args is None:
            args = []
        else:
            raise TypeError(f"Invalid type: {type(args)}")

        self.__args: list[str] = args

    @property
    def args(self) -> list[str]:
        return self.__args

    def __add__(self, other: str | Iterable[str] | Param) -> Param:
        return Param(self.__args + Param(other).args)

    def __iadd__(self, other: str | Iterable[str] | Param) -> Param:
        self = self + Param(other).args

        return self

    def __str__(self) -> str:
        return " ".join(self.__args)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({str(self)})"


class EnvVar(Param):
    def __init__(self, env_var: str, value: str) -> None:
        assert env_var.isidentifier(), f"Invalid env_var: '{env_var}'"
        assert len(shlex.split(value)) <= 1, f"Invalid value: '{value}'"

        super().__init__(f"--env={env_var}={value}")


class Volume(Param):
    def __init__(
        self, local_path: Path, volume_path: str, options: Optional[str] = None
    ) -> None:
        if not posixpath.isabs(volume_path):
            raise ValueError(f"volume_path='{volume_path}' shall be absolute")

        self.__local_path = local_path
        self.__docker_path = posixpath.normpath(volume_path)
        self.__volume_options = options

        super().__init__(
            f'--volume="{self.__local_path.absolute()}:{self.__docker_path}'
            f'{f":{self.__volume_options}" if self.__volume_options else ""}"'
        )

    @property
    def path(self) -> Path:
        return self.__local_path

    @property
    def volume_path(self) -> str:
        return self.__docker_path

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__qualname__}("
            f"{self.__local_path}:{self.__docker_path}"
            f"{':' + self.__volume_options if self.__volume_options else ''}"
            ")"
        )

    def joinpath(self, relative_path: str) -> Volume:
        if not self.__local_path.is_dir():
            raise ValueError(
                f"{self.__local_path} is not a directory, "
                "cannot join a relative path"
            )
        if posixpath.isabs(relative_path):
            raise ValueError(
                f"{relative_path} is absolute, "
                f"cannot be joined to {self.__local_path}"
            )

        return Volume(
            self.__local_path.joinpath(relative_path),
            f"{self.__docker_path}/{relative_path}",
            self.__volume_options,
        )

    def __truediv__(self, relative_path: str) -> Volume:
        return self.joinpath(relative_path)
